import React from "react";

class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {count: 0}
        this.increase = this.increase.bind(this)
    } 

    increase() {
        this.setState({count: this.state.count + 1})
    }

    componentDidMount() {
        console.log("Компонент смонтрирован")
      }

    componentDidUpdate() {
        console.log("Компонент обновлён")
    }

    componentWillUnmount() {
        console.log("Компонент размонтрирован")
    }

    render() {
        return(
            <div>
                Накликано: {this.state.count}
                <button onClick={this.increase}>+</button>
                
            </div>
        )
    }
}

export default Counter;