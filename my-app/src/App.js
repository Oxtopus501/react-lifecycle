import logo from './logo.svg';
import './App.css';
import React from 'react';
import Counter from './Counter';

let stringExample = "This is string";
let numberExample = 69;
let boolenExample = true;

let berserk = [
  "Что вершит судьбу человечества в этом мире?", 
  "Некое незримое существо или закон, подобно Длани Господней парящей над миром?", 
  "По крайне мере истинно то, что человек не властен даже над своей волей." 
];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      seconds: 0,
      isCounting: true
    }
    this.removeCounter = this.removeCounter.bind(this);
  }

  removeCounter() {
    this.setState({isCounting: false})
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Nice project. Awesome course.
          </p>
          <a
            className="App-link"
            href="https://www.youtube.com/watch?v=M37VucWh06Y"
            target="_blank"
            rel="noopener noreferrer"
          >
            Play suitable music
          </a>

          {this.state.isCounting ? <Counter /> : null}
          <button onClick={this.removeCounter}>Убрать счётчик</button>

          <p>{stringExample}</p>
          <p>{numberExample}</p>
          <p>{boolenExample}</p>
          <p>{null}</p>

          <ul>
            {berserk.map((phrase, index) => 
              <li key={index}>{phrase}</li>
            )}
          </ul>

        </header>
        
      </div>
    );
  }  
}

export default App;
